# TP6文件存储库 - 阿里云OSS驱动

> 基于 `chleniang/filesystem` 的**阿里云OSS驱动**

## 需求

- php >= 8.0.2 ( league/flysystem ^3.0的要求 )
- ThinkPHP >= 6.0 ( 建议使用6.1 移除了TP framework自带的filesystem )
- chleniang/filesystem
- aliyuncs/oss-sdk-php ~2.6

## 安装

`composer require "chleniang/filesystem-aliyun-oss"`

> 本驱动依赖库 `aliyuncs/oss-sdk-php` `~2.6`

[> 阿里云OSS官方文档 <](https://help.aliyun.com/document_detail/32098.html)

## 使用

  ```php
  # config/filesystem.php 文件中, 'disks'配置项添加 'aliyun-oss' 配置
    // 磁盘列表
    'disks'   => [
      ...,
      'aliyun' => [  // 磁盘名称,可以任意取(不重复即可)
        // 驱动类型,必须是 'aliyun-oss'
        'type' => 'aliyun-oss',
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。
        // 强烈建议您创建并使用RAM账号进行API访问或日常运维 登录 https://ram.console.aliyun.com 创建RAM账号
        'accessKeyId' => 'accessKeyIDaccessKeyID',
        'accessKeySecret' => 'accessKeySecretaccessKeySecret',
        // 访问域名 以杭州为例，"http://oss-cn-hangzhou.aliyuncs.com"
        'endpoint' => 'http://oss-cn-xxxxxx.aliyuncs.com',
        // 存储空间
        'bucket' => '',
        // CNAME是指将自定义域名绑定到存储空间上。使用自定义域名时，无法使用listBuckets方法
        // 默认false true为开启CNAME
        'isCName' => false,
        // 默认null from STS
        'securityToken' => null,
        // 默认null 代理服务器地址，例如 "http://<用户名>:<密码>@<代理ip>:<代理端口>"
        'requestProxy' => null,
        // Socket层传输数据的超时时间，单位秒; 默认5184000(要用默认值,设置为null即可)
        'timeout' => 3600,
        // 设置建立连接的超时时间，单位秒; 默认10(要用默认值,设置为null即可)
        'connectTimeout' => 10,
        // 前缀路径, 此处为迎合TP参数名没有使用 prefix
        'root' => '',
      ]
    ]
  
  # 使用
  \chleniang\filesystem\facade\Filesystem::disk('aliyun')-> ...
  ```

```text
    阿里云外网访问OSS URL格式
    <Schema>://<Bucket>.<外网Endpoint>/<Object>
        <Schema>：HTTP或者为HTTPS。
        <Bucket>：OSS存储空间名称。
        <外网Endpoint>：Bucket所在数据中心供外网访问的Endpoint，各地域Endpoint详情请参见访问域名和数据中心。
        <Object>：上传到OSS上的文件的访问路径。
```

> Bucket开启传输加速功能后，会增加如下传输加速Endpoint： [> 开启传输加速 <](https://help.aliyun.com/document_detail/131313.htm?spm=a2c4g.11186623.0.0.2a0160aesMQpZs#task-1813962)
> - 全球加速Endpoint：地址为 `oss-accelerate.aliyuncs.com` 传输加速接入点分布在全球各地，全球各地的Bucket均可以使用该域名进行传输加速。
> - 全球加速Endpoint：地址为 `oss-accelerate.aliyuncs.com` 传输加速接入点分布在全球各地，全球各地的Bucket均可以使用该域名进行传输加速。

## Lisence

- [Mulan PSL v2](http://license.coscl.org.cn/MulanPSL2)

