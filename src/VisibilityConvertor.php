<?php
// +------------------------------------------------------------------
// | chleniang\filesystem
// | Copyright (c) 2022 All rights reserved.
// | Based on ThinkPHP 6
// | Licensed MulanPSL2( http://license.coscl.org.cn/MulanPSL2 )
// | Author: CLS <422064377>
// | CreateDate: 2022/11/8
// +------------------------------------------------------------------

namespace chleniang\filesystem;

use League\Flysystem\InvalidVisibilityProvided;
use League\Flysystem\Visibility;
use OSS\OssClient;

class VisibilityConvertor
{
    /**
     * @param string $visibility
     *
     * @return string
     */
    public static function visibility2Acl(string $visibility): string
    {
        if (!in_array(
            $visibility,
            [
                Visibility::PRIVATE,
                Visibility::PUBLIC,
            ])
        ) {
            throw new InvalidVisibilityProvided('except "'
                . Visibility::PRIVATE . '" / "'
                . Visibility::PUBLIC
                . '" , but "' . $visibility . '" given.');
        }
        if ($visibility == Visibility::PRIVATE) {
            return OssClient::OSS_ACL_TYPE_PRIVATE;
        }
        return OssClient::OSS_ACL_TYPE_PUBLIC_READ;
    }

    /**
     * @param string $acl
     *
     * @return string
     */
    public static function acl2Visibility(string $acl): string
    {
        if (!in_array(
            $acl,
            [
                OssClient::OSS_ACL_TYPE_PRIVATE,
                OssClient::OSS_ACL_TYPE_PUBLIC_READ,
                OssClient::OSS_ACL_TYPE_PUBLIC_READ_WRITE,
            ])
        ) {
            throw new InvalidVisibilityProvided('except "'
                . OssClient::OSS_ACL_TYPE_PRIVATE . '" / "'
                . OssClient::OSS_ACL_TYPE_PUBLIC_READ . '" / "'
                . OssClient::OSS_ACL_TYPE_PUBLIC_READ_WRITE
                . '" , but "' . $acl . '" given.');
        }
        if ($acl == OssClient::OSS_ACL_TYPE_PRIVATE) {
            return Visibility::PRIVATE;
        }
        return Visibility::PUBLIC;
    }

}