<?php
// +------------------------------------------------------------------
// | chleniang/filesystem
// | Copyright (c) 2022 All rights reserved.
// | Based on ThinkPHP6.1
// | Licensed MulanPSL2( http://license.coscl.org.cn/MulanPSL2 )
// | Author: CLS <422064377>
// | CreateDate: 2022/10/31
// +------------------------------------------------------------------

namespace chleniang\filesystem\driver;


use chleniang\filesystem\AliyunOssAdapter;
use chleniang\filesystem\Driver;
use League\Flysystem\FilesystemAdapter;

class AliyunOss extends Driver
{
    /**
     * @var FilesystemAdapter
     */
    protected $adapter;

    protected function createAdapter(): FilesystemAdapter
    {
        $this->adapter = new AliyunOssAdapter(
            $this->config['accessKeyId'],
            $this->config['accessKeySecret'],
            $this->config['endpoint'],
            $this->config['bucket'],
            $this->config['isCName'] ?? false,
            $this->config['root'] ?? '',
            $this->config['securityToken'] ?? null,
            $this->config['requestProxy'] ?? null,
            $this->config['timeout'] ?? null,
            $this->connectTimeout['timeout'] ?? null
        );
        return $this->adapter;
    }

    /**
     * 获取URL
     *      迎合TP原有方法名
     *
     * @param string $path
     *
     * @return string
     */
    public function url(string $path): string
    {
        return $this->adapter->getUrl($path);
    }

}